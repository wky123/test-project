from flask import Flask, jsonify, make_response
import pymongo
import time


app = Flask(__name__)


collection = None

while collection is None:
    try:
        client = pymongo.MongoClient(host='mongo', port=27017)
        db = client.db
        collection = db['test_collection']
        print('connection succeeded')
    except:
        print('connection failed')
        time.sleep(1)


@app.route('/health', methods=['GET'])
def health():
    if collection is not None:
        try:
            collection.find_one({'_id': 'foo'})
            return make_response(jsonify({'healthy': True}), 200)
        except Exception as e:
            return make_response(jsonify({'healthy': False, 'error': str(e)}), 400)
    else:
        return make_response(jsonify({'healthy': False, 'error': 'Not ready'}), 400)

    
@app.route('/ping', methods=['POST'])
def ping():
    try:
        condition = {'_id': 'foo'}
        old_document = collection.find_one(condition)
        if old_document is not None:
            old_count = old_document['count']
        else:
            old_count = 0
        new_count = old_count + 1
        new_document = {'count': new_count}
        collection.replace_one(condition, new_document, upsert=True)
        return make_response(jsonify(new_document), 200)
    except Exception as e:
        return make_response(jsonify({'error': str(e)}), 500)



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
