FROM continuumio/anaconda3
RUN pip install pymongo[srv]
WORKDIR /usr/src/app
COPY app.py .
EXPOSE 80
CMD python app.py
