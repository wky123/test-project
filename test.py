import requests
import json
import time

HEALTH_URL = 'http://app:80/health'
URL = 'http://app:80/ping'


def await_healthy():
    while True:
        try:
            response = requests.get(HEALTH_URL)
            if response.status_code == 200:
                print('Healthy')
                return
            else:
                print('Not yet healthy')
                if response.status_code == 400:
                    response_json = json.loads(response.text)
                    print('Error = {}'.format(response_json['error']))
                time.sleep(1)
        except:
            print('Not yet healthy')
            time.sleep(1)
    

def test_endpoint(expected_count):
    response = requests.post(URL)
    print('Response status code = {}'.format(response.status_code))
    print('Response = {}'.format(response.text))
    assert response.status_code == 200
    response_json = json.loads(response.text)
    assert response is not None
    assert response_json['count'] == expected_count


if __name__ == '__main__':
    await_healthy()
    test_endpoint(expected_count=1)
    test_endpoint(expected_count=2)
